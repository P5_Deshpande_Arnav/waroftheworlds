package app;

import framework.SceneManager;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainApp extends Application {
	
	public void start(@SuppressWarnings("exports") Stage primaryStage) {
		try {
			
			SceneManager mgr = new SceneManager(primaryStage);
			
			mgr.switchScene( new framework.GameStartSceneContainer(mgr));
			
			//primaryStage.setScene(sc1.getScene());
			
			//primaryStage.show();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
