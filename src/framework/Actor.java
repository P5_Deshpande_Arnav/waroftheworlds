package framework;

import java.util.stream.Collectors;


/** This class is an abstract base class for general sprites in an arcade style game. */
public abstract class Actor extends javafx.scene.image.ImageView{

    /** Constructs an Actor */
    public Actor(){ super(); }

    /** This method is called every frame once start has been called. */
    public abstract void act(long now);

   
    public double getHeight(){ return super.getBoundsInLocal().getHeight(); }


    public <A extends Actor> java.util.List<A> getIntersectingObjects(java.lang.Class<A> cls){
     

        return this.getWorld().getChildren().stream()
                .filter(n -> n != this)
                .filter(n -> n.getBoundsInParent().intersects(this.getBoundsInParent()))
                .filter(n -> n.getClass() == cls)
                .map(cls::cast)
                .collect(Collectors.toList());
    }

  
    public <A extends Actor> A getOneIntersectingObject(java.lang.Class<A> cls){
    

        return this.getWorld().getChildren().stream()
                        .filter(n -> n != this)
                        .filter(n -> n.getBoundsInParent().intersects(this.getBoundsInParent()))
                        .filter(n -> n.getClass() == cls)
                        .map(cls::cast)
                .findFirst().orElse(null);
    }

  
    public double getWidth(){
        return super.getBoundsInLocal().getWidth();
    }

    
    public World getWorld(){
        return (World) super.getParent();
    }

  
    public void move(double dx, double dy){
        super.setX(super.getX() + dx);
        super.setY(super.getY() + dy);
    }
}
