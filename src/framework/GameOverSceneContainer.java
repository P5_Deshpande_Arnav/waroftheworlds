package framework;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

public class GameOverSceneContainer extends SceneContainer {
	
	final static double WIDTH = 1024;
	final static double HEIGHT = 512;
	
	private Cylinder cylinder;
	private PerspectiveCamera cam;
	
	public GameOverSceneContainer(SceneManager m)  {
		super(m);
	}
	
	@Override
	protected Scene createScene() {
	
		VBox vbox = new VBox();
        
        for (int i = 0; i < 30; i++) {
        	if (i%2 == 0) {
        		vbox.getChildren().add(new Text(" Game Over, ship is lost "  + " "));
        	}
        	else {
        		vbox.getChildren().add(new Text(" Game Over, Crew is dead "  + " "));
        	}
        }

        //take a sideways picture to fit the cylinder
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        snapshotParameters.setTransform(new Rotate(90));
        WritableImage snapshot = vbox.snapshot(snapshotParameters, null);

        //make sideways cyl with image
        PhongMaterial material = new PhongMaterial();
        cylinder = new Cylinder(500, snapshot.getWidth(),30);
        material.setDiffuseMap(snapshot);
        cylinder.setMaterial(material);
        cylinder.setRotate(-90);
        cylinder.setTranslateX(snapshot.getWidth());
        cylinder.setTranslateY(500);

        //lights camera show
        final Group root = new Group();
        root.getChildren().add(cylinder);
        
        String path = getClass().getResource("/resources/deep_blue.png").toString();
		Image img = new Image(path);
		BackgroundSize bSize = new BackgroundSize(WIDTH, HEIGHT, false, false, false, false); 
		BackgroundImage bgImg = new BackgroundImage(img,
	            BackgroundRepeat.NO_REPEAT,
	            BackgroundRepeat.NO_REPEAT,
	            BackgroundPosition.CENTER,
	            bSize);
		
		Background background2 = new Background(bgImg);
		vbox.setBackground(background2);

        final Scene scene = new Scene(root, snapshot.getWidth()*2, cylinder.getRadius()*2, true);
        PointLight pointLight = new PointLight(Color.ALICEBLUE);
        pointLight.setTranslateX(150);
        pointLight.setTranslateY(500);
        pointLight.setTranslateZ(-1000);
        cam = new PerspectiveCamera(false);
        scene.setCamera(cam);
        root.getChildren().addAll(pointLight, cam);

        return scene;
	}
	
	@Override
	public void start() {
		//I'll spin bob 
        Rotate rx = new Rotate();
        rx.setAxis(Rotate.Y_AXIS);
        cylinder.getTransforms().add(rx);
        cam.setRotationAxis(Point3D.ZERO);
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        final KeyValue kv = new KeyValue(rx.angleProperty(), -360);
        final KeyFrame kf = new KeyFrame(Duration.millis(10000), kv);
        timeline.getKeyFrames().add(kf);
        timeline.play();
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}
}