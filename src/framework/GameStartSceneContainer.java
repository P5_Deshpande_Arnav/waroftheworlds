package framework;
import javafx.scene.layout.VBox;
import javafx.scene.Scene;
import javafx.scene.control.Label;



public class GameStartSceneContainer extends SceneContainer {
	
	final static double WIDTH = 1024;
	final static double HEIGHT = 512;
	
	public GameStartSceneContainer(SceneManager m)  {
		super(m);
	}
	
	@Override
	protected Scene createScene() {
		
		Label label1= new Label("War Of the Worlds !!! Your mission is to survive");
		MyButton button1= new MyButton("GOTO Level 1");
		button1.layout();
		button1.requestLayout();
		button1.setMinWidth(250);
		
		button1.setOnAction((event) -> {    // lambda expression
			
			getSceneManager().switchScene(
					new framework.MeteorBeltSceneContainer(getSceneManager())
			);
	    });
		
		VBox layout1 = new VBox(20);     
		layout1.getChildren().addAll(label1, button1);
		
		return new Scene(layout1, WIDTH, HEIGHT);
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}
}
