package framework;

import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/*
 *  Class Meteor. Implements the meteor object. 
 */
public class Meteor extends Actor {
	
	// x and y distance that the meteor moves with every step. 
	private int dx;
	private int dy;
	
	// BorderPane that this meteor lives in
	private Pane root = null;
	
	double angle = 0;
	
	
	/* 
	 *  Constructor
	 *  @param v WorldController Object that controls the world behaviour. 
	 */
	public Meteor(SceneContainer sc) {
		super();
		String path = getClass().getResource("/resources/meteor_brown.png").toString();
		Image img = new Image(path);
		setImage(img);
		
		//controller = sc;
		
		angle = 0;
	}
	
	/*
	 * Setter for setting the BoarderPane Object
	 * @param bp BorderPane object
	 */
	public void setBorderPane(@SuppressWarnings("exports") Pane pane) {
		root =pane;
	}
	
	
	
	/*
	 * Setter for setting x movement
	 * @dx x position moved in each step
	 */
	public void setDx(int dx) {
		this.dx = dx;
	}
	
	/*
	 * Setter for setting y movement
	 * @dy y position moved in each step
	 */
	public void setDy(int dy) {
		this.dy = dy;
		
	}
	
	/*
	 * Meteor's Behavior for each step (Timer callback)
	 * @param now the time tick in nanoseconds representing current time since epoch.
	 */
	@Override
	public void act(long now) {
		
		String path = getClass().getResource("/resources/meteor_brown.png").toString();
		ImageView iv = new ImageView(new Image(path));
		
		angle += 1;
		if (angle > 360) {
			angle = 0;
		}
		
        iv.setRotate(angle);

        SnapshotParameters params = new SnapshotParameters();
        params.setFill(Color.TRANSPARENT);
        //Canvas canvas = new Canvas(100, 100);

        Image rotatedImage = iv.snapshot(params, null);
        setImage(rotatedImage);
        
		double x = getX();
		double y = getY();
		
		double newX = x + dx;
		double newY = y + dy;
		
		double minX = 0;
		double maxX = root.getWidth() - getWidth();
		double minY = 0;
		double maxY = root.getHeight() - getHeight();
			
		if (newX <= minX) {
			newX = minX;
			dx = dx * -1;
		}
			
		if (newX >= maxX) {
			newX = maxX;
			dx = dx * -1;
		}
			
		if (newY <= minY) {
			newY = minY;
			dy = dy * -1;
		}
			
		if (newY >= maxY) {
			newY = maxY;
			dy = dy * -1;
		}
		setX(newX);
		setY(newY);
	}

}
