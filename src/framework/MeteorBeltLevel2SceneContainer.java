

package framework;


public class MeteorBeltLevel2SceneContainer extends MeteorBeltSceneContainer {
	
		public MeteorBeltLevel2SceneContainer(SceneManager m)  {
		super(m);
	}
	
	@Override
	public void addMeteor() {
		if (!isHalted) {
			super.addMeteor();
			super.addMeteor();
		}
	}
	
	@Override
	public void updateScore(long score) {
		if (!isHalted) {
			setScore(score);
			if (score >= 200) {
				stop();
				getSceneManager().switchScene(
						new WinningSceneContainer(getSceneManager()));
			}
		}
	}
}

