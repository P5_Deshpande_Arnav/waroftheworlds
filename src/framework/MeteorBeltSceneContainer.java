package framework;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;

public class MeteorBeltSceneContainer extends SceneContainer  implements ProgressTracker {
	
	private static final int HEIGHT = 768;
	private static final int WIDTH = 1024;
	
	private BorderPane root;
	private World world;
	private framework.Ship ship;
	private Score scoreBoard;
	private Score hitBoard;
	
	protected boolean isHalted;
	
	private int cycle;
	
	public MeteorBeltSceneContainer(SceneManager m)  {
		super(m);
		cycle = 0;
		isHalted = true;
	}
	
	protected void add(Actor a) {
		world.stop();
		world.add(a);
		world.start();
	}
	protected void add(Node a) {
		world.stop();
		world.add(a);
		world.start();
	}
	
	protected void remove(Actor a) {
		world.stop();
		world.remove(a);
		world.start();
	}
	
	protected void remove(Node a) {
		world.stop();
		world.remove(a);
		world.start();
	}
	
	

	
	@Override
	protected Scene createScene() {
		root = new BorderPane();
		world = new MeteorWorld(this);
		root.setCenter(world);
		String path = getClass().getResource("/resources/deep_blue.png").toString();
		Image img = new Image(path);
		BackgroundSize bSize = new BackgroundSize(WIDTH, HEIGHT, false, false, false, false); 
		BackgroundImage bgImg = new BackgroundImage(img,
	            BackgroundRepeat.NO_REPEAT,
	            BackgroundRepeat.NO_REPEAT,
	            BackgroundPosition.CENTER,
	            bSize);
		
		Background background2 = new Background(bgImg);
		root.setBackground(background2);
		Scene mainScene = new Scene(root, WIDTH, HEIGHT );
		
		ship = new framework.Ship(this, this);
        ship.setX(150);
        ship.setY(500- ship.getHeight());
        ship.setBorderPane(root);
        add(ship);
        
        scoreBoard = new Score("Score: ");
        scoreBoard.setX(0);
        scoreBoard.setY(60);
        scoreBoard.setScore(0);
        add(scoreBoard);
        
        hitBoard = new Score("Ship Hit: ");
        hitBoard.setX(200);
        hitBoard.setY(60);
        add(hitBoard);
        
        for (int i = 0; i < 3; i++) {
	        Meteor meteor = new Meteor(this);
	        if (i == 0) {
	        	meteor.setDx(1);
	        	meteor.setDy(1);
	        }
	        
	        if (i == 1) {
	        	meteor.setDx(1);
	        	meteor.setDy(-1);
	        }
	        
	        if (i == 2) {
	        	meteor.setDx(-1);
	        	meteor.setDy(1);
	        }
	        meteor.setBorderPane(root);
	        meteor.setX(180 + 10*i);
	        meteor.setY(110 + 20*i);
	        add(meteor);
	        ship.getAvoidList().add(meteor);
	        ship.getTargetList().add(meteor);
        }
        
        world.setOnKeyPressed(event -> {
        	world.addKey(event.getCode());
        });
        
        world.setOnKeyReleased(event -> {
        	world.removeKey(event.getCode());
        });
        
        return mainScene;

	}
	
	
	public void addMeteor() {
		System.out.println("Adding Metero");
		Meteor meteor = new Meteor(this);
        meteor.setX(180 + 40);
        meteor.setY(110 + 30);
        if (cycle == 0) {
        	meteor.setDx(-2);
        	meteor.setDy(2);
        	cycle = 1;
        }
        else if (cycle == 1) {
        	meteor.setDx(2);
        	meteor.setDy(-2);
        	cycle = 2;
        }
        else if (cycle == 2) {
        	meteor.setDx(-2);
        	meteor.setDy(-2);
        	cycle = 3;
        }
        else {
        	meteor.setDx(2);
        	meteor.setDy(2);
        	cycle = 0;
        }
        
        meteor.setBorderPane(root);
        ship.getAvoidList().add(meteor);
        ship.getTargetList().add(meteor);
        add(meteor);
	}
	
	
	public void remove(Rocket r) {
		
		world.stop();
		world.remove(r);
		world.start();
	}
	
	public void remove(Meteor r) {
		
		world.stop();
		world.remove(r);
		world.start();
		
	}
	
	protected void setScore(long score) {
		scoreBoard.setScore(score);
	}
	
	@Override
	public void updateScore(long score) {
		// TODO Auto-generated method stub
		setScore(score);
		if (score > 200) {
			
			stop();
			isHalted = true;
			getSceneManager().switchScene(
					new framework.LevelOne2TwoTransitionSceneContainer(getSceneManager())
			);	
		}
	}

	@Override
	public void updatePenalty(long penalty) {
		if (!isHalted) {
			// TODO Auto-generated method stub
			hitBoard.setScore(penalty);
			if (penalty >= 3) {
				
				getSceneManager().switchScene(
						new framework.GameOverSceneContainer(getSceneManager())
				);
			}
		}
	}

	public void timeTick() {
		if (!isHalted) {
			addMeteor();
		}
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		world.start();
		world.requestFocus();
		isHalted = false;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		world.stop();	
		isHalted = true;
	}

}
