package framework;

public class MeteorWorld extends World {
	
	MeteorBeltSceneContainer mgr;
	long prev = 0;
	
	public MeteorWorld(MeteorBeltSceneContainer m) {
		mgr = m;
	}
	
	protected void finalize() throws Throwable {
		stop();
		mgr = null;
	}
	
	
	@Override
	public void act(long now) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void handleScene(long l) {
		// TODO Auto-generated method stub
		if ((l - prev) > 5e9) {
		
			if (mgr != null) {
				mgr.timeTick();
			}
			prev = l;
		}
		
	}

}
