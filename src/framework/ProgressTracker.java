package framework;

public interface ProgressTracker {
	
	void updateScore(long score);
	
	void updatePenalty(long penalty);
	
	//void complete(String msg, boolean success);
}
