package framework;

import javafx.scene.image.Image;

/*
 * Class Rocket
 * Rocket object that can be fired from the ship
 */
public class Rocket extends Actor {
	
	MeteorBeltSceneContainer mgr;
	
	boolean outOfBounds;
	
	
	public Rocket(MeteorBeltSceneContainer sc) {
		super();
		String path = getClass().getResource("/resources/grey_circle.png").toString();
		Image img = new Image(path);
		setImage(img);
		mgr = sc;
		outOfBounds = false;
	}
	
	public boolean isOutOfBounds() {
		return outOfBounds;
	}
	
	
	@Override
	public void act(long now) {
		if (outOfBounds) {
			return;
		}
		
		double y = getY();
		
		double newY = y - 1;
		
		
		if (newY > 0) {
			setY(newY);
		}
		else {
			mgr.remove(this);
			this.setVisible(false);
			outOfBounds = true;
		}
	}

}
