package framework;

import javafx.scene.Scene;

public abstract class SceneContainer { 

	//private World world;
	private Scene sc;
	private SceneManager sm;
	
	public SceneContainer(SceneManager m) {
		
		//world = null;
		sc = createScene();
		sm = m;
		
		//world.stop();
	}
	
	@SuppressWarnings("exports")
	public Scene getScene() {
		return sc;
	}
	
	protected SceneManager getSceneManager() {
		return sm;
	}
	
	protected void finalize() throws Throwable {
		stop();
	}
	
	abstract protected Scene createScene();
	
	abstract public void start();
	
	abstract public void stop();
	
}
