package framework;

import javafx.stage.Stage;

public class SceneManager {
	
	private Stage mainStage;
	private SceneContainer currentSceneContainer;
	
	public SceneManager(@SuppressWarnings("exports") Stage s) {
		mainStage = s;
		currentSceneContainer = null;
		
	}
	
	public void switchScene(SceneContainer sc) {
		
		if (currentSceneContainer != null) {
			currentSceneContainer.stop();
		}
		mainStage.hide();
		mainStage.setScene(sc.getScene());
		currentSceneContainer = sc;
		currentSceneContainer.start();
		mainStage.show();
	}

}
