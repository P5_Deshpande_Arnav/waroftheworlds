package framework;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Score extends Text {
	
	private long score;
	private String text;
	
	public Score(String t) {
		score = 0;
		text = t;
		this.setFont(Font.font ("Verdana", 20));
		this.setStroke(Color.WHITE); 
		updateDisplay();
	}
	
	public void setScore(long score) {
		this.score = score;
		updateDisplay();
	}
	
	
	
	public void setMessage(String msg) {
		this.setFont(Font.font ("Verdana", 50));
		this.setStroke(Color.WHITE); 
		this.setText(text + msg);
	}
	
	public void updateDisplay() {
		this.setText(text + Long.toString(score));
	}
	

}
