package framework;

import java.util.ArrayList;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;

public class Ship extends Actor {
	
	BorderPane root = null;
	MeteorBeltSceneContainer mgr = null;
	ProgressTracker tracker = null;
	
	long damage;
	long score;
	
	private ArrayList<Actor> toAvoid;
	private ArrayList<Actor> toHit;
	private ArrayList<Rocket> firedRockets;
	
	
	public Ship(MeteorBeltSceneContainer sc, ProgressTracker tracker) {
		super();
		String path = getClass().getResource("/resources/red_ship.png").toString();
		Image img = new Image(path);
		setImage(img);
		mgr = sc;
		this.tracker = tracker;
		
		toAvoid = new ArrayList<>();
		toHit = new ArrayList<>();
		firedRockets = new ArrayList<>();
		score = 0;
		damage = 0;
	}
	
	public void setBorderPane(@SuppressWarnings("exports") BorderPane x) {
		root =x;
	}
	
	public ArrayList<Actor> getAvoidList() {
		return toAvoid;
	}
	
	public ArrayList<Actor> getTargetList() {
		return toHit;
	}
	
	private boolean isHit(Actor a1, Actor a2) {
		boolean collisionDetected = false;

		if (a1.getBoundsInParent().intersects(a2.getBoundsInParent())) {
			collisionDetected = true;
		}

		return collisionDetected;
	}
	
	
	@Override
	public void act(long now) {
		
		double minX = 0;
		double maxX = 500;
		double minY = 0;
		double maxY = 500;
		
		if (root != null) {
			minX = 0;
			maxX = root.getWidth();
			minY = 0;
			maxY = root.getHeight();
		}
		
		ArrayList<Actor> removeListHit = new ArrayList<>();
		ArrayList<Actor> removeListAvoid = new ArrayList<>();
		ArrayList<Rocket> removeListRocket = new ArrayList<>();
		// Check if any object that we must avoid
		for (Actor m : toAvoid) {
			
			if (isHit(this, m)) {
				damage++;
				removeListAvoid.add(m);
				tracker.updatePenalty(damage);
				//if (damage >= 3) {
				//	mgr.complete("Game Over: Ship destroyed", false);
				//}
			}
		}
		// Check if any rocket hit meteor
		for (Rocket b: firedRockets) {
			for (Actor a: toHit) {
				if (isHit(b, a)) {
					removeListHit.add(a);
					removeListRocket.add(b);
					score += 10;
					tracker.updateScore(score);
				}
			}
		}
		
		for (Actor a :removeListAvoid ) {
			a.setVisible(false);
			mgr.remove(a);
			toAvoid.remove(a);
		}
		
		for (Actor a :removeListHit ) {
			a.setVisible(false);
			mgr.remove(a);
			toHit.remove(a);
		}
		
		for (Rocket b :removeListRocket ) {
			b.setVisible(false);
			mgr.remove(b);
			firedRockets.remove(b);
		}
		
		
		if (getWorld().isKeyDown(KeyCode.LEFT)) {
			double currentX = getX();
			double newX = currentX - 3;
    		if (newX > minX) {
    			setX(newX);
    		}
		}
		else if (getWorld().isKeyDown(KeyCode.RIGHT)) {
			double currentX = getX();
			double newX = currentX + 3;
    		if (newX < maxX) {
    			setX(newX);
    		}
		}
		else if (getWorld().isKeyDown(KeyCode.SPACE)) {
			
			Rocket r = new Rocket(mgr);
	        r.setX(getX() + (getWidth()/4));
	        r.setY(getY() - (getHeight()/2));
	      
	        mgr.add(r);
	        firedRockets.add(r);
	        System.out.println("Fired Rocket");
			getWorld().clearKey(KeyCode.SPACE);
		}
		else if (getWorld().isKeyDown(KeyCode.UP)) {
			
			double currentY = getY();
			double newY = currentY - 3;
    		if (newY > minY) {
    			setY(newY);
    		}
		}
		else if (getWorld().isKeyDown(KeyCode.DOWN)) {
			
			double currentY = getY();
			double newY = currentY + 3;
    		if (newY < maxY) {
    			setY(newY);
    		}
		}
	}

}
