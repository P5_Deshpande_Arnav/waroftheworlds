package framework;

import java.util.stream.Collectors;
import javafx.animation.AnimationTimer;
import java.util.List;

import javafx.scene.Node;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.HashSet;  


/** This class is an extension of a Pane that holds Actors (which are custom ImageView objects) */
public abstract class World extends javafx.scene.layout.AnchorPane{
    /** Animation timer that helps move the object */
    private final AnimationTimer timer;
    
    private ArrayList<Actor> actors;
    
    private ArrayList<Node> nodes;
    
    private HashSet<KeyCode> keyDown;
    
    private long prevTime = 0;
    
    private boolean isRunning;
    
    protected void InitializeActors() {
    	
    	super.getChildren().clear();
    	
    	for (Actor a : actors) {
    		super.getChildren().add(a); 
    	}
    	
    	for (Node n: nodes) {
    		super.getChildren().add(n); 
    	}
    	
    }

    /** Constructs a World */
    public World(){
        super();

        actors = new ArrayList<>();
        
        nodes = new ArrayList<>();
        
        keyDown = new HashSet<>();
        
        isRunning = false;
        
        this.timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
            	
            	if (isRunning) {
	                if (l - prevTime > 1e3) {
	                	//World.super.getChildren().forEach(n -> ((Actor) n).act(l));
	                	InitializeActors();
	                	
	                	for (Node n : World.super.getChildren()) {
	                		handleScene(l);
	                		if (n instanceof Actor) {
	                			((Actor) n).act(l);
	                		}
	                	}
	                	prevTime = l;
	                }
            	}
            }
        };
        
    }
    
    protected abstract void handleScene(long l);

    /** This method is called every frame once start has been called. */
    public abstract void act(long now);

    /** Adds the given actor to the world.
     * @param actor The actor to add to the world.
     */
    public void add(Actor actor){ 
    	
    	//super.getChildren().add(actor); 
    	//System.out.println("Adding Actor");
    	actors.add(actor);
    }

    public void add(@SuppressWarnings("exports") Node n){ 
    	
    	//super.getChildren().add(actor); 
    	//System.out.println("Adding Node");
    	nodes.add(n);
    }

    /** Returns a list of all the actors in the world of the given class.
     * @param cls The class of the objects that the method will return
     * @return a list of all the actors in the world of the given class.
     * */
    public <A extends Actor> List<A> getObjects(Class<A> cls){
        //Uses the Stream API to convert the observableList returned by getChildren()
        //into a stream, filters out all objects not of the given class,
        //and returns a list with only objects of the given class.

        return super.getChildren().stream()
                .filter(n -> n.getClass() == cls)
                .map(cls::cast)
                .collect(Collectors.toList());
    }

    /** Removes the given actor from the world.
     *  @param actor The actor to remove from the world.
     * */
    public void remove(Actor actor){ 
    	
    	if (actors.remove(actor)) {
    		//System.out.println("Removing Actor");
    	}
    	
    	//super.getChildren().remove(actor); 
    }

    public void remove(@SuppressWarnings("exports") Node n){ 
    	
    	if (nodes.remove(n)) {
    		//System.out.println("Remving Node");
    	}
    	
    	//super.getChildren().remove(actor); 
    }

    /** Starts the timer that calls the act method on the world and on each Actor in the world each frame. */
    public void start(){ isRunning = true; this.timer.start();  }

    /** Stops the timer that calls the act method. */
    public void stop(){ isRunning = false; this.timer.stop();  }
    
    public void addKey(@SuppressWarnings("exports") KeyCode key) {
    	if (!keyDown.contains(key)) {
    		keyDown.add(key);
    	}
    }
    
    public void removeKey(@SuppressWarnings("exports") KeyCode key) {
    	keyDown.remove(key);
    }
    
    public boolean isKeyDown(@SuppressWarnings("exports") KeyCode key) {
    	if (keyDown.contains(key) ) {
    		return true;
    	}
    	return false;
    }
    
    public void clearKey(@SuppressWarnings("exports") KeyCode key) {
    	while (keyDown.contains(key)) {
    		keyDown.remove(key);
    	}
    }
}
