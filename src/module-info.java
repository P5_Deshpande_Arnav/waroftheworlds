module war.world {
	requires javafx.graphics;
	requires javafx.base;
	requires javafx.controls;
	exports app;
	opens app to javafx.graphics;
	exports framework;
	opens framework to javafx.graphics;
	requires transitive java.compiler;
	requires javafx.media;
}